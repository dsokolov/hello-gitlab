package me.ilich.hellogitlab;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MathTest {

    private Math math;

    @Before
    public void setUp() {
        math = new Math();
    }

    @Test
    public void sumInt() {
        assertEquals(5, math.sum(1, 4));
    }

}
