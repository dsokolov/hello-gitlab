package me.ilich.hellogitlab;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private EditText fieldA;
    private EditText fieldB;
    private EditText fieldResult;

    private Math math = new Math();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fieldA = (EditText) findViewById(R.id.field_a);
        fieldB = (EditText) findViewById(R.id.field_b);
        fieldResult = (EditText) findViewById(R.id.field_result);
    }

    public void onSumClick(View view) {
        String a = fieldA.getText().toString();
        String b = fieldB.getText().toString();
        String c = math.sum(a, b);
        fieldResult.setText(c);
    }

}
